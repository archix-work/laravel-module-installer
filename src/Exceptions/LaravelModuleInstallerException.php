<?php

namespace Archix\LaravelModuleInstaller\Exceptions;

use Exception;

class LaravelModuleInstallerException extends Exception
{
    /**
     * Invalid package format.
     *
     * @param  string  $invalidPackageName
     *
     * @return static
     */
    public static function fromInvalidPackage(string $invalidPackageName): self
    {
        return new self(
            "Ensure your package's name ({$invalidPackageName}) is in the format <vendor>/<name>-<module>"
        );
    }
}
