<?php

namespace Archix\LaravelModuleInstaller;

use Composer\Composer;
use Composer\IO\IOInterface;
use Composer\Plugin\PluginInterface;

class LaravelModuleInstallerPlugin implements PluginInterface
{
    /**
     * Active Module.
     *
     * @param  Composer  $composer
     * @param  IOInterface  $io
     */
    public function activate(Composer $composer, IOInterface $io): void
    {
        $installer = new LaravelModuleInstaller($io, $composer);
        $composer->getInstallationManager()->addInstaller($installer);
    }

    /**
     * Deactivate Module.
     *
     * @param  Composer  $composer
     * @param  IOInterface  $io
     */
    public function deactivate(Composer $composer, IOInterface $io): void
    {
        $installer = new LaravelModuleInstaller($io, $composer);
        $composer->getInstallationManager()->removeInstaller($installer);
    }

    /**
     * Uninstall Module.
     *
     * @param  Composer  $composer
     * @param  IOInterface  $io
     */
    public function uninstall(Composer $composer, IOInterface $io): void
    {
        $installer = new LaravelModuleInstaller($io, $composer);
        $composer->getInstallationManager()->removeInstaller($installer);
    }
}
